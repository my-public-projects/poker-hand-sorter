# Poker Hand Sorter

Requirement: [poker-exercise.pdf](./PokerHandSorter/poker-exercise.pdf)

Input Data: [poker-hands.txt](./PokerHandSorter/poker-hands.txt)

Execute program:

```Bat
cd PokerHandSorter
dotnet run poker-hands.txt
```