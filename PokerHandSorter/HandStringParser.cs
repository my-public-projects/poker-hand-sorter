﻿class HandStringParser
{
    public List<Card> GetCards(string handString)
    {
        string[] cardStrings = handString.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        return cardStrings.Select(s => new Card(s)).ToList();
    }
};
