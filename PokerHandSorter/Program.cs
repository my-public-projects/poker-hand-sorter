﻿try
{
    string filePath = Environment.GetCommandLineArgs()[1];

    if (!File.Exists(filePath))
    {
        Console.WriteLine($"Cannot find file: {filePath}");
        return;
    }

    Console.WriteLine($"Processing: {filePath}");

    using (StreamReader sr = System.IO.File.OpenText(filePath))
    {
        var comparer = new HandComparer();

        string? line = null;
        int counter1 = 0;
        int counter2 = 0;
        while (( line = sr.ReadLine()) != null)
        {
            string resultMessage = "";
            switch(comparer.Compare(line))
            {
                case 0:
                    resultMessage = $"{comparer.Rank1} vs {comparer.Rank2}, tie!";
                    break;
                case -1:
                    resultMessage = $"{comparer.Rank1} vs {comparer.Rank2}, player 1 wins, total {++counter1} hands";
                    break;
                case 1:
                    resultMessage = $"{comparer.Rank1} vs {comparer.Rank2}, player 2 wins, total {++counter2} hands";
                    break;
            }
            Console.WriteLine($"{line}: {resultMessage}");
        }

        Console.WriteLine($"\nFinal result:");
        Console.WriteLine($"player 1: {counter1} hands");
        Console.WriteLine($"player 2: {counter2} hands");
    }
}
catch (Exception ex)
{
    Console.WriteLine(ex.ToString()); 
}