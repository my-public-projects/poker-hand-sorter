﻿using PokerHandSorter;
using PokerHandSorter.RankMatching;

public class HandComparer
{
    HandStringParser _parser = new HandStringParser();
    RankMarker _marker = new RankMarker();

    public int Rank1 { get; set; }
    public int Rank2 { get; set; }
    public int HighestValue { get; set; }

    public int Compare(string handString)
    {
        var cards = _parser.GetCards(handString);

        if (cards == null || cards.Count != 10)
            throw new InvalidCardNumberException();

        var mark_player1 = _marker.GetMark(cards.Take(5).ToList());
        var mark_player2 = _marker.GetMark(cards.Skip(5).Take(5).ToList());

        Rank1 = mark_player1.Rank;
        Rank2 = mark_player2.Rank;

        // compare rank
        if(mark_player1.Rank > mark_player2.Rank)
            return -1;
        else if(mark_player1.Rank < mark_player2.Rank)
            return 1;
        else
        {
            // compare highest value
            for (int i = 0; i < 5; i++)
            {
                var comparison = mark_player1.HighestValues[i] - mark_player2.HighestValues[i];
                if (comparison == 0)
                    continue;
                else if (comparison > 0)
                {
                    HighestValue = mark_player1.HighestValues[i];
                    return -1;
                }
                else
                {
                    HighestValue = mark_player2.HighestValues[i];
                    return 1;
                }
            }

            return 0;
        }
    }
}
