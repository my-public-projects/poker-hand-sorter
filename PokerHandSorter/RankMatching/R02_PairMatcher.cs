﻿namespace PokerHandSorter.RankMatching
{
    /// <summary>
    /// Pair: Two cards of same value
    /// </summary>
    public class R02_PairMatcher : RankMatcher, IRankMatcher
    {
        private void Calc2ndHighestValue(int excludedValue)
        {
            _orderedCards.RemoveAll(c => c.Value == excludedValue);
            var remains = _orderedCards.OrderBy(c => c.Value).ToArray();
            HighestValues[1] = remains[2].Value;
            HighestValues[2] = remains[1].Value;
            HighestValues[3] = remains[0].Value;

        }
        protected override bool InternalMatch()
        {
            var group = OrderedCards.GroupBy(x => x.Value)
                                    .Where(g => g.Count() == 2)
                                    .Select(y => new { Element = y.Key, Counter = y.Count() })
                                    .ToList();
            if (group.Count() == 1)
            {
                HighestValues[0] = group[0].Element;
                Calc2ndHighestValue(HighestValues[0]);

                _rank = 2;
                return true;
            }
            else
                return false;
        }
    }
}
