﻿namespace PokerHandSorter.RankMatching
{
    /// <summary>
    /// Three of a kind: Three cards of the same value
    /// </summary>
    public class R04_Three_of_a_Kind_Matcher : RankMatcher, IRankMatcher
    {
        private void Calc2ndAnd3rdHighestValue(int excludedValue)
        {
            _orderedCards.RemoveAll(c => c.Value == excludedValue);

            if(_orderedCards[0].Value > _orderedCards[1].Value)
            {
                HighestValues[1] = _orderedCards[0].Value;
                HighestValues[2] = _orderedCards[1].Value;
            }
            else
            {
                HighestValues[1] = _orderedCards[1].Value;
                HighestValues[2] = _orderedCards[0].Value;
            }
        }
        protected override bool InternalMatch()
        {
            var group = OrderedCards.GroupBy(x => x.Value)
                                    .Select(y => new { Element = y.Key, Counter = y.Count() })
                                    .ToList();

            if (group.Count() == 3)
            {
                var groupOf3 = group.Find(x => x.Counter == 3);
                if (groupOf3 != null)
                {
                    _rank = 4;
                    HighestValues[0] = groupOf3.Element;
                    Calc2ndAnd3rdHighestValue(HighestValues[0]);
                    return true;
                }
            }

            return false;
        }
    }
}
