﻿namespace PokerHandSorter.RankMatching;

/// <summary>
/// Straight flush: All five cards in consecutive value order, with the same suit
/// </summary>
class R09_StraightFlushMatcher : RankMatcher, IRankMatcher
{
    protected override bool InternalMatch()
    {
        if (IsConsecutive() && isFlush())
        {
            _rank = 9;
            HighestValues[0] = _orderedCards.Last().Value;
            return true;
        }
        else
            return false;
    }
}