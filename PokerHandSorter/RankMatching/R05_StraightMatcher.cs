﻿namespace PokerHandSorter.RankMatching
{
    /// <summary>
    /// Straight: All five cards in consecutive value order
    /// </summary>
    public class R05_StraightMatcher : RankMatcher, IRankMatcher
    {
        protected override bool InternalMatch()
        {
            if(IsConsecutive())
            {
                HighestValues[0] = _orderedCards.Last().Value;
                _rank = 5;
                return true;
            }
            else
                return false;
        }
    }
}
