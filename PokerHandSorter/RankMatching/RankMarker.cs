﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerHandSorter.RankMatching
{
    public class RankMarker
    {
        IRankMatcher[] matchers = {
            new R10_RoyalFlushMatcher(),
            new R09_StraightFlushMatcher(),
            new R08_Four_of_a_Kind_Matcher(),
            new R07_FullHouseMatcher(),
            new R06_FlushMatcher(),
            new R05_StraightMatcher(),
            new R04_Three_of_a_Kind_Matcher(),
            new R03_TwoPairsMatcher(),
            new R02_PairMatcher(),
            new R01_HighCardMatcher(),
        };

        /// <returns>(rank, hightest_value)</returns>
        public HandValues GetMark(List<Card> cards)
        {
            for (int i = 0; i < matchers.Length; i++)
            {
                matchers[i].OrderedCards = cards;
                if (matchers[i].Match())
                {
                    var matcher = matchers[i];
                    return new HandValues(matcher.GetRank(), matcher.HighestValues );
                }
            }

            //return new HandValues(0,0,0,0);
            return new HandValues();
        }


    }
}
