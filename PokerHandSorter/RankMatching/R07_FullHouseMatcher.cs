﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerHandSorter.RankMatching
{
    /// <summary>
    /// Flush house: Three of a kind and a Pair
    /// </summary>
    public class R07_FullHouseMatcher : RankMatcher, IRankMatcher
    {
        private int Calc2ndHighestValue(int excludedValue)
        {
            _orderedCards.RemoveAll(c => c.Value == excludedValue);
            return _orderedCards.First()!.Value;
        }

        protected override bool InternalMatch()
        {
            var group = OrderedCards.GroupBy(x => x.Value)
                                    .Where(g => g.Count() > 1)
                                    .Select(y => new { Element = y.Key, Counter = y.Count() })
                                    .ToList();
            if (group.Count() == 2)
            {
                var groupOf3 = group.Find(x => x.Counter == 3);
                if (groupOf3 != null)
                {
                    HighestValues[0] = groupOf3.Element;
                    HighestValues[1] = Calc2ndHighestValue(HighestValues[0]);
                    _rank = 7;
                    return true;
                }
                else
                    return false;   // it might be 2 tied pairs
            }
            else
                return false;
        }
    }
}
