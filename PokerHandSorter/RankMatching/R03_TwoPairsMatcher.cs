﻿namespace PokerHandSorter.RankMatching
{
    /// <summary>
    /// Two Pairs: Two different pairs
    /// </summary>
    public class R03_TwoPairsMatcher : RankMatcher, IRankMatcher
    {
        private int Calc2ndHighestValue(int excludedValue1, int excludedValue2)
        {
            _orderedCards.RemoveAll(c => c.Value == excludedValue1 || c.Value == excludedValue2);
            return _orderedCards.First()!.Value;
        }

        protected override bool InternalMatch()
        {
            var group = OrderedCards.GroupBy(x => x.Value)
                                    .Where(g => g.Count() == 2)
                                    .Select(y => new { Element = y.Key, Counter = y.Count() })
                                    .ToList();
            if (group.Count() == 2)
            {
                _rank = 3;
                if(group[0].Element > group[1].Element)
                {
                    HighestValues[0] = group[0].Element;
                    HighestValues[1] = group[1].Element;
                }
                else
                {
                    HighestValues[0] = group[1].Element;
                    HighestValues[1] = group[0].Element;
                }
                HighestValues[2] = Calc2ndHighestValue(HighestValues[0], HighestValues[1]);
                return true;
            }
            else
                return false;
        }
    }
}
