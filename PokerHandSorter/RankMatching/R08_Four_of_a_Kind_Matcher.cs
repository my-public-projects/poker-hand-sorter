﻿namespace PokerHandSorter.RankMatching;

/// <summary>
/// Four of a kind: Four cards of the same value
/// </summary>
public class R08_Four_of_a_Kind_Matcher : RankMatcher, IRankMatcher
{
    protected override bool InternalMatch()
    {
        var group = OrderedCards.GroupBy(x => x.Value)
                                .Where(g => g.Count() == 4)
                                .Select(y => new { Element = y.Key, Counter = y.Count() })
                                .ToList();

        if(group.Count() == 1)
        {
            HighestValues[0] = group[0].Element;
            HighestValues[1] = _orderedCards.Find(c => c.Value != HighestValues[0])!.Value;
            return true;
        }
        else
            return false;
    }
}
