﻿namespace PokerHandSorter.RankMatching;

public abstract class RankMatcher : IRankMatcher
{
    protected List<Card> _orderedCards = new List<Card>();
    public List<Card> OrderedCards {
        set { _orderedCards = value.OrderBy(c => c.Value).ToList(); }
        get { return _orderedCards; }
    }
    protected int _rank = 0;

    public int GetRank() { return _rank; }

    // lower index stands for higher rank groups
    public int[] HighestValues { get; set; }  = new int[5] { 0, 0, 0, 0, 0 };

    public int GetHighestValue(int index)
    {
        if(index < 0 || index >= HighestValues.Length)
            throw new ArgumentOutOfRangeException($"index = {index}");

        return HighestValues[index];
    }

    public bool Match()
    {
        if (!hasValidCardNumber())
            throw new InvalidCardNumberException();
        return InternalMatch();
    }

    protected abstract bool InternalMatch();

    protected bool hasValidCardNumber()
    {
        if (OrderedCards.Count == 5)
            return true;
        else
            return false;
    }

    protected bool IsConsecutive()
    {
        for (int i = 0; i < OrderedCards.Count - 1; i++)
        {
            if (OrderedCards[i + 1].Value != OrderedCards[i].Value + 1) return false;
        }

        return true;
    }
    
    protected bool isFlush()
    {
        for (int i = 0; i < OrderedCards.Count - 1; i++)
        {
            if (OrderedCards[i].Suit != OrderedCards[i + 1].Suit) return false;
        }

        return true;
    }
}
