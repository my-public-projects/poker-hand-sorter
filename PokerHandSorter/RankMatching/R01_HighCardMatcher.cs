﻿namespace PokerHandSorter.RankMatching
{
    public class R01_HighCardMatcher : RankMatcher, IRankMatcher
    {
        protected override bool InternalMatch()
        {
            var group = OrderedCards.GroupBy(x => x.Value).ToList();

            if (group.Count() == 5)
            {
                HighestValues[0] = _orderedCards[4].Value;
                HighestValues[1] = _orderedCards[3].Value;
                HighestValues[2] = _orderedCards[2].Value;
                HighestValues[3] = _orderedCards[1].Value;
                HighestValues[4] = _orderedCards[0].Value;
                _rank = 1;
                return true;
            }
            else
                return false;
        }
    }
}
