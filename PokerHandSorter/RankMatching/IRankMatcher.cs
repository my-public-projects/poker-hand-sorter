﻿namespace PokerHandSorter.RankMatching;

interface IRankMatcher
{
    bool Match();
    int GetRank();

    int GetHighestValue(int index);
    List<Card> OrderedCards { get; set; }

    // lower index stands for higher rank groups
    int[] HighestValues { get; set; }
}
