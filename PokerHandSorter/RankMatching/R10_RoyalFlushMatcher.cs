﻿namespace PokerHandSorter.RankMatching;

/// <summary>
/// Royal Flush: Ten, Jack, Queen, King and Ace in the same suit
/// </summary>
class R10_RoyalFlushMatcher : RankMatcher, IRankMatcher
{
    protected override bool InternalMatch()
    {
        if (OrderedCards[0].Value == 10 && IsConsecutive() && isFlush())
        {
            _rank = 10;
            return true;
        }
        else
            return false;
    }
}
