﻿namespace PokerHandSorter.RankMatching
{
    /// <summary>
    /// Flush: All five cards having the same suit
    /// </summary>
    public class R06_FlushMatcher : RankMatcher, IRankMatcher
    {
        protected override bool InternalMatch()
        {
            var group = OrderedCards.GroupBy(x => x.Value)
                        .Select(y => new { Element = y.Key, Counter = y.Count() })
                        .ToList();

            if(group.Count() != 5)
                return false;

            for (int i = 0; i < OrderedCards.Count - 1; i++)
            {
                if (OrderedCards[i].Suit != OrderedCards[i + 1].Suit)
                    return false;
            }

            var cardValues = _orderedCards.Select(x => x.Value).OrderByDescending(x => x).ToArray();

            for(int i = 0; i < cardValues.Length; i++)
                HighestValues[i] = cardValues[i];

            _rank = 6;
            return true;
        }
    }
}
