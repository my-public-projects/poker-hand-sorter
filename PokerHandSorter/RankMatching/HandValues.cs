﻿namespace PokerHandSorter.RankMatching
{
    public class HandValues
    {
        public int Rank { get; set; } = 0;
        public int[] HighestValues { get; } = new int[5] { 0,0,0,0,0 };

        public HandValues() { }
        public HandValues(int rank, int[] highestValues)
        {
            if(highestValues.Length != HighestValues.Length)
                throw new ArgumentOutOfRangeException(nameof(highestValues));

            Rank = rank;
            for (int i = 0; i < highestValues.Length; i++)
                HighestValues[i] = highestValues[i];
        }

    }
}
