﻿
using PokerHandSorter;

public class Card
{
    public int Value { get; set; }
    public char Suit { get; set; }

    public Card(string cardString)
    {
        cardString = cardString.Trim().ToUpper();
        if (cardString.Length != 2) throw new InvalidCardStringException();

        switch (cardString[0])
        {
            case 'T':
                Value = 10;
                break;
            case 'J':
                Value = 11;
                break;
            case 'Q':
                Value = 12;
                break;
            case 'K':
                Value = 13;
                break;
            case 'A':
                Value = 14;
                break;
            default:
                Value = int.Parse($"{cardString[0]}");
                break;
        }
        Suit = cardString[1];
    }
}
