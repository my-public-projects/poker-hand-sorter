﻿using PokerHandSorter.xUnit.Utils;
using Xunit;

namespace PokerHandSorter.xUnit
{
    public class HandComparerTests_R01_HighCard : HandComparerBase
    {

        [Fact]
        public void P1_should_win__highest_a()
        {
            var result = comparer.Compare("5D 8C 9S JS AC       2C 5C 7D 8S QH"); // highest: p1/a
            Assert.Equal(14, comparer.HighestValue);
            Assert.Equal(-1, result);
            Assert.Equal(1, comparer.Rank1);
            Assert.Equal(1, comparer.Rank2);
        }

        [Fact]
        public void P1_should_win__highest_9()
        {
            var result = comparer.Compare("5D 8C 9S JS AC       2C 5C JD 8S AH"); // highest: p1/9
            Assert.Equal(9, comparer.HighestValue);
            Assert.Equal(-1, result);
            Assert.Equal(1, comparer.Rank1);
            Assert.Equal(1, comparer.Rank2);
        }

        [Fact]
        public void P1_should_win__highest_5()
        {
            var result = comparer.Compare("5D 8C 9S JS AC       2C 8D JD 9D AH"); // highest: p1/5
            Assert.Equal(5, comparer.HighestValue);
            Assert.Equal(-1, result);
            Assert.Equal(1, comparer.Rank1);
            Assert.Equal(1, comparer.Rank2);
        }
    }
}