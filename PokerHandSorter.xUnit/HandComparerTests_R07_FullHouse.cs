﻿using PokerHandSorter.xUnit.Utils;
using Xunit;

namespace PokerHandSorter.xUnit
{
    public class HandComparerTests_R07_FullHouse : HandComparerBase
    { 
        [Fact]
        public void P1_should_win__different_three_of_kind()
        {
            var result = comparer.Compare("2H 2D 4C 4D 4S       3C 3D 3S 9S 9D"); // (3 fours, 2 twos) vs (3 threes, 2 nines)
            Assert.Equal(-1, result);
            Assert.Equal(4, comparer.HighestValue);
            Assert.Equal(7, comparer.Rank1);
            Assert.Equal(7, comparer.Rank2);
        }

        [Fact]
        public void P2_should_win__different_pair()     // this is actuall invalid for one deck of cards
        {
            var result = comparer.Compare("2H 2D 4C 4D 4S       4C 4D 4S 9S 9D"); // (3 fours, 2 twos) vs (3 fours, 2 nines)
            Assert.Equal(1, result);
            Assert.Equal(9, comparer.HighestValue);
            Assert.Equal(7, comparer.Rank1);
            Assert.Equal(7, comparer.Rank2);
        }
    }
}