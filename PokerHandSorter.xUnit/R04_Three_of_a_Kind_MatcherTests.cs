﻿using PokerHandSorter.RankMatching;
using System.Collections.Generic;
using Xunit;

namespace PokerHandSorter.xUnit
{
    public class R04_Three_of_a_Kind_MatcherTests
    {
        [Fact]
        public void InternalMatch_should_work()
        {
            var matcher = new R04_Three_of_a_Kind_Matcher();

            matcher.OrderedCards = new List<Card> {
                new Card("4H"),
                new Card("4C"),
                new Card("6S"),
                new Card("9S"),
                new Card("4D"),
            };
            Assert.True(matcher.Match());
            Assert.Equal(4, matcher.GetHighestValue(0));
            Assert.Equal(9, matcher.GetHighestValue(1));
            Assert.Equal(6, matcher.GetHighestValue(2));

            matcher.OrderedCards = new List<Card> {  // This is NOT three of a kind but a full house
                new Card("4H"),
                new Card("4C"),
                new Card("6S"),
                new Card("6S"),
                new Card("4D"),
            };
            Assert.False(matcher.Match());
        }
    }
}