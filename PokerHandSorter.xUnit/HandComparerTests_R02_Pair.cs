﻿using PokerHandSorter.xUnit.Utils;
using Xunit;

namespace PokerHandSorter.xUnit
{
    public class HandComparerTests_R02_Pair : HandComparerBase
    {

        [Fact]
        public void should_tie()
        {
            var result = comparer.Compare("4H 4C 6S 7S KD       4H 4C 6S 7S KD"); // (2 fours, 6, 7, k) vs (2 fours, 6, 7, k), highest: 0
            Assert.Equal(0, comparer.HighestValue);
            Assert.Equal(0, result);
            Assert.Equal(2, comparer.Rank1);
            Assert.Equal(2, comparer.Rank2);

        }

        [Fact]
        public void P1_should_wins__same_pairs()
        {
            var result = comparer.Compare("4D 6S 9H QH QC       3D 6D 7H QD QS");     // (2 Qs, 4, 6, 9) vs (2 Qs, 3, 6, 7), highest: p1/9
            Assert.Equal(9, comparer.HighestValue);
            Assert.Equal(-1, result);
            Assert.Equal(2, comparer.Rank1);
            Assert.Equal(2, comparer.Rank2);
        }

        [Fact]
        public void P2_should_wins__same_pairs()
        {
            var result = comparer.Compare("3D 6S 9H QH QC       4D 6D 9D QD QS");     // (2 Qs, 3, 6, 9) vs (2 Qs, 4, 6, 9), highest: p2/4
            Assert.Equal(4, comparer.HighestValue);
            Assert.Equal(1, result);
            Assert.Equal(2, comparer.Rank1);
            Assert.Equal(2, comparer.Rank2);
        }

        [Fact]
        public void P2_should_win__different_pairs()
        {
            var result = comparer.Compare("4H 4C 6S 7S KD       2C 3S 9S 9D TD");     // (2 fours, k, 7, 6) vs (2 nines, 10, 3, 2), highest: p2/9
            Assert.Equal(9, comparer.HighestValue);
            Assert.Equal(1, result);
            Assert.Equal(2, comparer.Rank1);
            Assert.Equal(2, comparer.Rank2);
        }
    }
}