﻿using PokerHandSorter.xUnit.Utils;
using Xunit;

namespace PokerHandSorter.xUnit
{
    public class handComparerTests_R04_ThreeOfAKind : HandComparerBase
    {
        [Fact]
        public void P1_should_win__different_three()
        {
            var result = comparer.Compare("2H 7D 4C 4D 4S       3C 3D 3S 6S 9D"); // (3 fours, 7, 2) vs (3 threes, 9, 6)
            Assert.Equal(-1, result);
            Assert.Equal(4, comparer.HighestValue);
            Assert.Equal(4, comparer.Rank1);
            Assert.Equal(4, comparer.Rank2);
        }

        [Fact]
        public void P2_should_win__same_three__case1()
        {
            var result = comparer.Compare("8H 9S 4C 4D 4S       4C 4D 4S 6S 9D"); // (3 fours, 9, 8) vs (3 fours, 9, 6)
            Assert.Equal(-1, result);
            Assert.Equal(8, comparer.HighestValue);
            Assert.Equal(4, comparer.Rank1);
            Assert.Equal(4, comparer.Rank2);
        }

        [Fact]
        public void P2_should_win__same_three__case2()
        {
            var result = comparer.Compare("2H 7D 4C 4D 4S       4C 4D 4S 6S 9D"); // (3 fours, 7, 2) vs (3 fours, 9, 6)
            Assert.Equal(1, result);
            Assert.Equal(9, comparer.HighestValue);
            Assert.Equal(4, comparer.Rank1);
            Assert.Equal(4, comparer.Rank2);
        }

    }
}