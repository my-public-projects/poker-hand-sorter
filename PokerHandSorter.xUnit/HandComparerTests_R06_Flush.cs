﻿using PokerHandSorter.xUnit.Utils;
using Xunit;

namespace PokerHandSorter.xUnit
{
    public class HandComparerTests_R06_Flush : HandComparerBase
    {
        [Fact]
        public void P2_should_win__different_rank()
        {
            var result = comparer.Compare("2D 9C AS AH AC       3D 6D 7D TD QD"); // (3 fours, 2 twos) vs (flush D)
            Assert.Equal(4, comparer.Rank1);
            Assert.Equal(6, comparer.Rank2);
            Assert.Equal(0, comparer.HighestValue);
            Assert.Equal(1, result);
        }

        [Fact]
        public void Tie__different_flush_same_values()
        {
            var result = comparer.Compare("3H 6H 7H TH QH       3D 6D 7D TD QD"); // (flush H) vs (flush D)
            Assert.Equal(6, comparer.Rank1);
            Assert.Equal(6, comparer.Rank2);
            Assert.Equal(0, comparer.HighestValue);
            Assert.Equal(0, result);
        }
        [Fact]
        public void P1_should_win__different_flush()
        {
            var result = comparer.Compare("4H 6H 7H TH QH       3D 6D 7D TD QD"); // (flush H) vs (flush D)
            Assert.Equal(6, comparer.Rank1);
            Assert.Equal(6, comparer.Rank2);
            Assert.Equal(4, comparer.HighestValue);
            Assert.Equal(-1, result);
        }
    }
}