﻿using PokerHandSorter.xUnit.Utils;
using Xunit;

namespace PokerHandSorter.xUnit
{
    public class HandComparerTests_R03_TwoPairs : HandComparerBase
    { 
        [Fact]
        public void P2_should_win__same_pairs()
        {
            var result = comparer.Compare("2H 2D 4C 4D 7S       2C 2S 4H 4S 9D"); // (2 fours, 2 twos, 7) vs (2 fours, 2 twos, 9), highest: p2/9
            Assert.Equal(1, result);
            Assert.Equal(3, comparer.Rank1);
            Assert.Equal(3, comparer.Rank2);
        }

        [Fact]
        public void P2_should_win__diff_pairs()
        {
            var result = comparer.Compare("2H 2D 4C 4D 7S       3C 3S 4H 4S 9D"); // (2 fours, 2 twos, 7) vs (2 fours, 2 threes, 9)
            Assert.Equal(1, result);
            Assert.Equal(3, comparer.Rank1);
            Assert.Equal(3, comparer.Rank2);
        }

        [Fact]
        public void P1_should_win__diff_pairs()
        {
            var result = comparer.Compare("8H 8D 4C 4D 7S       3C 3S 4H 4S 9D"); // (2 eights, 2 fours, 7) vs (2 fours, 2 threes, 9)
            Assert.Equal(-1, result);
            Assert.Equal(3, comparer.Rank1);
            Assert.Equal(3, comparer.Rank2);
        }
    }
}