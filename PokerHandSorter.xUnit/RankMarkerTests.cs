﻿using PokerHandSorter.RankMatching;
using System.Collections.Generic;
using Xunit;

namespace PokerHandSorter.xUnit
{
    public class RankMarkerTests
    {
        [Fact]
        public void Royal_flush_Should_rank_10()
        {
            var marker = new RankMarker();

            Assert.Equal(10, marker.GetMark(new List<Card> {
                new Card("TH"),
                new Card("JH"),
                new Card("QH"),
                new Card("KH"),
                new Card("AH"),
            }).Rank);
        }

        [Fact]
        public void Strait_Should_rank_5()
        {
            var marker = new RankMarker();

            Assert.Equal(5, marker.GetMark(new List<Card> {
                new Card("TD"),
                new Card("JH"),
                new Card("QH"),
                new Card("KH"),
                new Card("AH"),
            }).Rank);
        }
    }
}