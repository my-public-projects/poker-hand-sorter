﻿using PokerHandSorter.RankMatching;
using System.Collections.Generic;
using Xunit;

namespace PokerHandSorter.xUnit
{
    public class R08_Four_of_a_Kind_MatcherTests
    {
        [Fact]
        public void InternalMatch_should_work()
        {
            var matcher = new R08_Four_of_a_Kind_Matcher();
            matcher.OrderedCards = new List<Card> {
                new Card("4H"),
                new Card("4C"),
                new Card("6S"),
                new Card("4S"),
                new Card("4D"),
            };

            Assert.True(matcher.Match());
            Assert.Equal(4, matcher.GetHighestValue(0));
            Assert.Equal(6, matcher.GetHighestValue(1));
            Assert.Equal(0, matcher.GetHighestValue(2));     // not available actually
        }
    }
}