﻿using PokerHandSorter.RankMatching;
using System.Collections.Generic;
using Xunit;

namespace PokerHandSorter.xUnit
{
    public class R06_FlushMatcherTests
    {
        [Fact]
        public void InternalMatch_should_be_true()
        {
            var matcher = new R06_FlushMatcher();
            matcher.OrderedCards = new List<Card> {
                new Card("4H"),
                new Card("JH"),
                new Card("QH"),
                new Card("8H"),
                new Card("3H"),
            };
            Assert.True(matcher.Match());
            Assert.Equal(12, matcher.GetHighestValue(0));
            Assert.Equal(11, matcher.GetHighestValue(1));
            Assert.Equal(8, matcher.GetHighestValue(2));
        }
    }
}